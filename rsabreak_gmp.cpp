/*
 * RSABreak GMP
 * 
 * Just a simple C++ program that (brute force) breaks a given RSA public key.
 * Uses GMP, so go install that first.
 * Because of this, there is no limitation on integer sizes.
 * However, it is noticebly slower than the 64-bit Integer version.
 * 
 * Add "-lgmpxx -lgmp" to your build command
 * 
 * Ryan Foster (phasecorex@gmail.com)
 */

#include <iostream>		// I/O - Cout <3
#include <stdio.h>		// I/O - Standard
#include <math.h>		// System - Math
#include <gmp.h>		// System - Better Math

using namespace std;

mpz_t e, n, d, p, q, phin, negaone, test, count;

/*
 * Main
 */
int main(int argc, char **argv){
	
	mpz_init(e);
	mpz_init(n);
	mpz_init(d);
	mpz_init(p);
	mpz_init(q);
	mpz_init(phin);
	mpz_init(negaone);
	mpz_init(test);
	mpz_init(count);
	
	mpz_set_si (negaone, -1);
	
	string temp;
	switch(argc){
		case 3:
			mpz_set_str(e, argv[1], 10);
			mpz_set_str(n, argv[2], 10);
			break;
		default:
			break;
	}
	while(e==0){
		cout << "\nEnter RSA key part E: ";
		getline (cin,temp);
		mpz_set_str(e, temp.c_str(), 10);
	}
	while(n==0){
		cout << "Enter RSA key part N: ";
		getline (cin,temp);
		mpz_set_str(n, temp.c_str(), 10);
	}
	cout << "\nPerforming brute force attack on key:\n(" << e << ", " << n << ")";
	fflush(stdout);	
	mpz_sqrt (count, n);
	mpz_mod_ui (test, count, 2);
	if(mpz_cmp_d (test, 0)==0)	// Deal with the possibility of count being an even number
		mpz_add_ui (count, count, 1);
	while(mpz_cmp_d (p, 0)==0&&mpz_cmp_d (count, 1)>0){
		mpz_mod (test, n, count);
		if(mpz_cmp_d (test, 0)==0)
			mpz_cdiv_q (p, n, count);
		mpz_sub_ui (count, count, 2);
		// I want to add some sort of an ETA feature, but I don't know how to without slowing it down a bunch.
	}
	if(mpz_cmp_d (p, 0)!=0){
		mpz_cdiv_q (q, n, p);
		mpz_sub_ui (p, p, 1);
		mpz_sub_ui (q, q, 1);
		mpz_mul (phin, p, q);
		mpz_powm (d, e, negaone, phin);
		cout << "\n\nBroken!\nP:\t";
		mpz_out_str (stdout, 10, p);
		cout << "\nQ:\t";
		mpz_out_str (stdout, 10, q);
		cout << "\n\nN:\t";
		mpz_out_str (stdout, 10, n);
		cout << "\nPhi(n):\t";
		mpz_out_str (stdout, 10, phin);
		cout <<  "\n\nE:\t";
		mpz_out_str (stdout, 10, e);
		cout << "\nD:\t";
		mpz_out_str (stdout, 10, d);
		cout <<  "\n\n";
	}
	else{
		cout << "\nThis RSA key does not seem to be valid...\nN was not able to be factored into 2 prime numbers.\n\n";
	}
	mpz_clear(e);
	mpz_clear(n);
	mpz_clear(d);
	mpz_clear(p);
	mpz_clear(q);
	mpz_clear(phin);
	mpz_clear(negaone);
	mpz_clear(test);
	mpz_clear(count);
}
