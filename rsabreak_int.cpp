/*
 * RSABreak 64-bit Integer
 * 
 * Just a simple C++ program that (brute force) breaks a given RSA public key.
 * Requires no external libraries.
 * Because of this, there is a 64-bit limitation on integer sizes.
 * (n cannot be greater than 18,446,744,073,709,551,615)
 * Also, it is much faster than the GMP version.
 * 
 * Ryan Foster (phasecorex@gmail.com)
 */

#include <iostream>		// I/O - Cout <3
#include <stdio.h>		// I/O - Standard
#include <sstream>		// I/O - StringStream
#include <math.h>		// System - Math

using namespace std;

unsigned long long int e = 0;
unsigned long long int n = 0;
unsigned long long int d = 0;
unsigned long long int p = 0;
unsigned long long int q = 0;

/*
 * Returns the gcd of a and b followed by the pair x and y of equation ax + by = gcd(a,b)
 */
pair<unsigned long long int, pair<unsigned long long int, unsigned long long int> > extendedEuclid(unsigned long long int a, unsigned long long int b) {
    if(a == 0) return make_pair(b, make_pair(0, 1));
    pair<unsigned long long int, pair<unsigned long long int, unsigned long long int> > p;
    p = extendedEuclid(b % a, a);
    return make_pair(p.first, make_pair(p.second.second - p.second.first*(b/a), p.second.first));
}

/*
 * Returns the result of a^(-1) mod m
 */
unsigned long long int modInverse(unsigned long long int a, unsigned long long int m) {
    return (extendedEuclid(a,m).second.first + m) % m;
}

/*
 * Main
 */
int main(int argc, char **argv){
	string temp;
	switch(argc){
		case 3:
			istringstream (argv[1]) >> e;
			istringstream (argv[2]) >> n;
			break;
		default:
			break;
	}
	while(e==0){
		cout << "\nEnter RSA key part E: ";
		getline (cin,temp);
		istringstream (temp) >> e;
	}
	while(n==0){
		cout << "Enter RSA key part N: ";
		getline (cin,temp);
		istringstream (temp) >> n;
	}
	cout << "\nPerforming brute force attack on key:\n(" << e << ", " << n << ")";
	fflush(stdout);
	unsigned long long int count = floorl(sqrtl(n));
	while(p==0&&count>1){
		if(n%count==0)
			p=n/count;
		count=count-2;
		// I want to add some sort of a ETA feature, but I don't know how to without slowing it down a bunch.
	}
	if(p!=0){
		q=n/p;
		d=modInverse(e,(p-1)*(q-1));
		cout << "\n\nBroken!\nP:\t" << p << "\nQ:\t" << q << "\n\nN:\t" << n << "\nPhi(n):\t" << (p-1)*(q-1) << "\n\nE:\t" << e << "\nD:\t" << d << "\n\n";
	}
	else{
		cout << "\nThis RSA key does not seem to be valid...\nN was not able to be factored into 2 prime numbers.\n\n";
	}
}
